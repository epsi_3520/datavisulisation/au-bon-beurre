import process_production as process
import time
import numpy as np

if __name__ == '__main__':
    print("Produce")
    
    mode_production = "produce and send"
    need_unblock = 0
    
    while True:
        
        if mode_production == "produce and send":
            bad_working_proof = np.random.binomial(n=1, p=0.4) == 1
            result = process.produce(bad_working_proof=bad_working_proof, need_unblock = need_unblock)
            if result == False:
                mode_production = "produce"
            else:
                need_unblock = 0
        
        else:
            print("""
                  --------------------------------------------------------------------------
                  ------------------------ Try to unblock collector ------------------------
                  --------------------------------------------------------------------------
                  """)
            mode_production = "produce and send"
            need_unblock = 1
            time.sleep(10)
        
        time.sleep(5)