import json
import random
import datetime
import socket
import pickle
import ast
import numpy as np

from Crypto.PublicKey import RSA
from Crypto.Random import get_random_bytes
from Crypto.Cipher import AES, PKCS1_OAEP

unit_number = 1
listChoice = []
for x in range(10):
    listChoice.append(x+1)      
listAutomate=random.sample(listChoice, 10)

listAutomateType = []
for i in listAutomate:
    type = random.randint(0,16)
    if type > 9 :
        charType = chr(91+type)
    else:
        charType = str(type)
    
    listAutomateType.append("0X0000BA2" + charType)

def generate_data (bad_working_proof):
    
    result = dict()
    result["unit_number"] = unit_number
    result["data"] = []
    
    for i in range(len(listAutomate)):
        
        # erreur de mesure
        tank_temperature_error = 0.5
        external_temperature_error = 2
        ph_error = 0.2
        milk_weight_error = 500
       
        # génération de la données
        automate_data = {}
        automate_data["automate_number"] = listAutomate[i]
        automate_data["automate_type"] = listAutomateType[i]
        automate_data["tank_temperature"]=round(random.uniform(2.5 - tank_temperature_error, 4 + tank_temperature_error), 1)
        automate_data["external_temperature"]=round(random.uniform(8 - external_temperature_error, 14 + external_temperature_error), 1)
        automate_data["milk_weight"]=random.randint(3512 - milk_weight_error, 4607 + milk_weight_error)
        automate_data["ph"]=round(random.uniform(6.8 - ph_error, 7.2 + ph_error), 1)
        automate_data["k_plus"]=random.randint(35, 47)
        automate_data["nacl"]=round(random.uniform(1, 1.7), 1)
        automate_data["salmonella"]=random.randint(17, 37)
        automate_data["e_coli"]=random.randint(35, 49)
        automate_data["listeria"]=random.randint(28, 54)
        
        result["data"].append(automate_data)

    date_epoch_int = round(datetime.datetime.now().timestamp())
    fileName = f"paramunite_{result['unit_number']}_{date_epoch_int}.json"
    date_epoch_binary = "{0:b}".format(date_epoch_int)
    
    key_binary = "10111001100000110011000011"
    # right-padding 0
    key_binary_padded = key_binary + "0"*(len(date_epoch_binary)-len(key_binary))
    key_int = int(key_binary_padded, 2)
    
    working_proof = date_epoch_int ^ key_int
    working_proof = '{0:0{1}b}'.format(working_proof,len(date_epoch_binary))
    
    if (bad_working_proof is True):
        result["working_proof"] = working_proof+"0"
        print("[XOR] Create working proof with error")
    else:
        result["working_proof"] = working_proof
        print("[XOR] Create working proof")
    
    with open(fileName, 'w') as file:
        json.dump(result, file)
        print("****************** File is created ***************************")
    
    return fileName

def encrypt(data):
    
    # get AES and RSA public key
    aes_key = get_random_bytes(16)
    rsa_key_pub = RSA.import_key(open("pub.pem").read())
    
    # encrypt the AES key with RSA public key
    rsa_encription = PKCS1_OAEP.new(rsa_key_pub)
    aes_key_encrypted = rsa_encription.encrypt(aes_key)
    
    # encrypt the message with the AES key
    aes_encryption = AES.new(aes_key, AES.MODE_EAX)
    data_encrypted, tag = aes_encryption.encrypt_and_digest(data)
     
    return aes_key_encrypted, aes_encryption.nonce, tag, data_encrypted

def produce(bad_working_proof, need_unblock): 
    
    fileName = generate_data(bad_working_proof)
    
    HOST = 'controller'  # The server's hostname or IP address
    PORT = 4000        # The port used by the server
    
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.connect((HOST, PORT))
    
    with open(fileName, 'rb') as file:
        data = file.read()
        
    # encrypt the data with public key
    aes_key_encrypted, aes_encryption_nonce, tag, data_encrypted = encrypt(data)
    print("[RSA/AES] Encrypt Data with RSA/AES")
    message = (fileName, aes_key_encrypted, aes_encryption_nonce, tag, data_encrypted, need_unblock)
    
    # Sending the message to the server
    print(f"[SOCKET] Sending file : {fileName}")
    message_encoded = pickle.dumps(message)
    soc.send(message_encoded)
    # Receive the confirmation from the controller
    message_collector = soc.recv(4096).decode()
    
    print(message_collector)
    
    soc.close()
    
    count = 1 
    while "Working proof is not valid" in message_collector and count < 3:
        count = count + 1
        
        is_working_proof_fixed = np.random.binomial(n=1, p=0.3) == 1
        
        soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        soc.connect((HOST, PORT))
        
        if is_working_proof_fixed:
            
            data = json.loads(data)
            
            data["working_proof"] = data["working_proof"][:-1]
            
            data = json.dumps(data).encode()
                
            # encrypt the data with public key
            aes_key_encrypted, aes_encryption_nonce, tag, data_encrypted = encrypt(data)
        
        
        print("[XOR] Working proof is not valid. Re-send the data with good working proof")
        
        message = (fileName, aes_key_encrypted, aes_encryption_nonce, tag, data_encrypted, need_unblock)
        
        # Sending the message to the server
        print("----------------------------------------------------------")
        print(f"[SOCKET] Re-sending file : {fileName}")
        message_encoded = pickle.dumps(message)
        soc.send(message_encoded)
        # Receive the confirmation from the controller
        message_collector = soc.recv(4096).decode()
        print(message_collector)
        
        soc.close()
    
    if count == 3:
        return False
    return True

