from django.db import models

class BonBeurre(models.Model):
    unit_number = models.IntegerField(blank=True, null=True)
    automate_number = models.IntegerField(blank=True, null=True)
    automate_type = models.CharField(max_length=20, blank=True, null=True)
    tank_temperature = models.FloatField(blank=True, null=True)
    external_temperature = models.FloatField(blank=True, null=True)
    milk_weight = models.IntegerField(blank=True, null=True)
    ph = models.FloatField(blank=True, null=True)
    k_plus = models.IntegerField(blank=True, null=True)
    nacl = models.FloatField(blank=True, null=True)
    salmonella = models.IntegerField(blank=True, null=True)
    e_coli = models.IntegerField(blank=True, null=True)
    listeria = models.IntegerField(blank=True, null=True)
    produced_at = models.DateTimeField(blank=True, null=True)
    collected_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'bon_beurre'
        