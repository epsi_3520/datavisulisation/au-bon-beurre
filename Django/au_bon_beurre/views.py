from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from au_bon_beurre.models import BonBeurre
import time


# Create your views here.
def home_page(request):

    return render(request, "au_bon_beurre/home_page.html")


def graph(request, site, automate, mesure):
    data = []
    label = []
    date = BonBeurre.objects.filter(unit_number = site, automate_number = automate).values("produced_at")
    value = BonBeurre.objects.filter(unit_number = site, automate_number = automate).values(mesure)
    
    date_string = []
    date_epoch = []
    for d in date:
        date_string.append(d["produced_at"].strftime("%d/%m/%y %H:%M:%S"))
        date_epoch.append(round(d["produced_at"].timestamp()))
    
    start = date_epoch[0]
    end = date_epoch[-1]
    
    for D in range(start, end + 5):
        label.append(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(D)))
        
        if D in date_epoch:
            data.append(value[date_epoch.index(D)][mesure])
        else:
            data.append(0)
    

    return render(request, "au_bon_beurre/graph.html", {"label": label, "data": data})
    