from django.apps import AppConfig


class AuBonBeurreConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'au_bon_beurre'
