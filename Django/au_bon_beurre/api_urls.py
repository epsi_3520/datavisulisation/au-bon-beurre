from django.urls import path
from . import views

app_name = 'au_bon_beurre'

urlpatterns = [
    path('', views.home_page),
    path('chart/site/<int:site>/automate/<int:automate>/mesure/<str:mesure>', views.graph, name='graph'),
]



