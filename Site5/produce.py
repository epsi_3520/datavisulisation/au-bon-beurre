# -*- coding: utf-8 -*-

import json
import random
import datetime
import socket


def generate_data ():
    
    result = dict()
    result["unit_number"] = unit_number
    result["data"] = []
    
    for i in range(len(listAutomate)):
       
        automate_data = {}
        automate_data["automate_number"] = listAutomate[i]
        automate_data["automate_type"] = listAutomateType[i]
        automate_data["tank_temperature"]=round(random.uniform(2.5, 4), 1)
        automate_data["external_temperature"]=round(random.uniform(8, 14), 1)
        automate_data["milk_weight"]=random.randint(3512, 4607)
        automate_data["ph"]=round(random.uniform(6.8, 7.2), 1)
        automate_data["k_plus"]=random.randint(35, 47)
        automate_data["nacl"]=round(random.uniform(1, 1.7), 1)
        automate_data["salmonella"]=random.randint(17, 37)
        automate_data["e_coli"]=random.randint(35, 49)
        automate_data["listeria"]=random.randint(28, 54)
        
        result["data"].append(automate_data)

    fileName = f"paramunite_{result['unit_number']}_{datetime.datetime.now().strftime('%Y_%m_%d-%H_%M_%S')}.json"
    
    with open(fileName, 'w') as file:
        json.dump(result, file)
        print("****************** File is created ***************************")
    
    return fileName

def produce(): 
    
    fileName = generate_data()
    
    HOST = 'controller'  # The server's hostname or IP address
    PORT = 4000        # The port used by the server
    
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        
        with open(fileName, 'rb') as file:
            data = file.read()
            
            # Sending the filename to the controller
            s.send(fileName.encode())
            # Receive the confirmation from the controller
            message = s.recv(4096).decode()
            print(message)
            
            # Sending the file to the server
            s.send(data)
            # Receive the confirmation from the controller
            message = s.recv(4096).decode()
            print(message)
                
if __name__ == '__main__':
    
    unit_number = 5
    listChoice = []
    for x in range(10):
        listChoice.append(x+1)      
    listAutomate=random.sample(listChoice, 10)
    
    listAutomateType = []
    for i in listAutomate:
        type = random.randint(0,16)
        if type > 9 :
            charType = chr(91+type)
        else:
            charType = str(type)
        
        listAutomateType.append("0X0000BA2" + charType)
    
    print("Produce")
    produce()