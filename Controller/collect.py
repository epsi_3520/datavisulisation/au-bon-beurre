import mysql.connector
import socket
import ast
import pickle
import datetime
import random

from Crypto.PublicKey import RSA
from Crypto.Cipher import AES, PKCS1_OAEP

def collect ():
    
    key_site_1 = "10111001100000110011000011"
    
    mydb = mysql.connector.connect(
          host="db",
          port=3306,
          user="root",
          password="root",
          database="bon_beurre"
    )
    
    cursor = mydb.cursor()

    HOST = 'controller'  # Standard loopback interface address (localhost)
    PORT = 4000        # Port to listen on (non-privileged ports are > 1023)
    
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(10)
    
    site1_failed = 0;
    
    while True:
        conn, addr = s.accept()
        print ("-------------------------------------------------------------")
        with conn:
            print('Connected by', addr)
            
            # Receive the message from client
            message_encoded = conn.recv(4096)
            message = pickle.loads(message_encoded)
            fileName, aes_key_encrypted, aes_encryption_nonce, tag, data_encrypted, need_unblock = message
            print(f"[SOCKET] Receiving the {fileName}.")
            
            # decrypt the AES key with private key
            rsa_key_private = RSA.import_key(open("private.pem").read())
            rsa_encription = PKCS1_OAEP.new(rsa_key_private)
            aes_key = rsa_encription.decrypt(aes_key_encrypted)
            
            # decrypt the data_encrypted
            aes_encryption = AES.new(aes_key, AES.MODE_EAX, aes_encryption_nonce)
            file = aes_encryption.decrypt_and_verify(data_encrypted, tag).decode('utf-8')
            print("[RSA/AES] Decrypt the data in file")

            # Parse and save data in DB
            file = ast.literal_eval(file)
            
            # check the working proof        
            date = fileName.split(".")[0]
            date_epoch_int = int(date.split("_",2)[2])
            working_proof = file["working_proof"]
            
            if file["unit_number"] == 1 :
                if site1_failed < 3:
                    key_binary = key_site_1
                    if check_working_proof(key_binary, date_epoch_int, working_proof) == True:
                        # insert data
                        print("[XOR] Working proof is valid")
                        insert_data(file, fileName, cursor, mydb)
                        # Send a confirmation to the client
                        conn.send("Data is saved in DB.".encode())
                        # reset site1_failed
                        site1_failed = 0
                    else:
                        print("[XOR] Working proof is NOT valid !!!!!!")
                        conn.send("Working proof is not valid. \nPlease send the data with correct working proof".encode())
                        
                        site1_failed = site1_failed + 1 
                else:
                    conn.send("Site 1 is BLOCKED !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!".encode()) 
                    site1_failed = 0
                    key_binary = key_site_1
                    if check_working_proof(key_binary, date_epoch_int, working_proof) == True:
                        # insert data
                        print("[XOR] Working proof is valid")
                        insert_data(file, fileName, cursor, mydb)
                        # Send a confirmation to the client
                        conn.send("Data is saved in DB.".encode())
                        # reset site1_failed
                        site1_failed = 0
                    else:
                        print("[XOR] Working proof is NOT valid !!!!!!")
                        conn.send("Working proof is not valid. \nPlease send the data with correct working proof".encode())
                        
                        site1_failed = site1_failed + 1 
            
            
                    
                    
            
                
                
            


def check_working_proof(key_binary, date_epoch_int, working_proof):
    date_epoch_binary = "{0:b}".format(date_epoch_int)
    
    # right-padding 0
    key_binary_padded = key_binary + "0"*(len(date_epoch_binary)-len(key_binary))
    key_int = int(key_binary_padded, 2)
    
    working_proof_computed = date_epoch_int ^ key_int
    working_proof_computed = '{0:0{1}b}'.format(working_proof_computed,len(date_epoch_binary))
    
    if(working_proof_computed == working_proof):
        return True
    
    return False


# fileName = "paramunite_2_1347517370.json"
def insert_data(file, fileName, cursor, mydb):
    
    date = fileName.split(".")[0]
    epoch = date.split("_",2)[2]

    
    produced_at = datetime.datetime.fromtimestamp(int(epoch)).strftime('%Y-%m-%d %H:%M:%S')
    
    unit_number = file["unit_number"]
    
    data = file["data"]
    
    for automate in data:
        automate_number         = automate['automate_number']
        automate_type           = automate['automate_type']
        tank_temperature        = automate['tank_temperature']
        external_temperature    = automate['external_temperature']
        milk_weight             = automate['milk_weight']
        ph                      = automate['ph']
        k_plus                  = automate['k_plus']
        nacl                    = automate['nacl']
        salmonella              = automate['salmonella']
        e_coli                  = automate['e_coli']
        listeria                = automate['listeria']
        
        # k_plus
        if ph > 7.2:
            print(f"[ETL] Automate {automate_number} ph = {ph} > 7.2, proceed to modify this value")
            ph = round(random.uniform(7, 7.2), 1)   
        if ph < 6.8:
            print(f"[ETL] Automate {automate_number} ph = {ph} < 6.8, proceed to modify this value")
            ph = round(random.uniform(6.8, 7.2), 1)
            
        # température cuve
        if tank_temperature < 2.5 or tank_temperature > 4:
            print(f"[ETL] Automate {automate_number} tank_temperature {tank_temperature} is not in (2.5; 4), proceed to modify this value")
            tank_temperature = round(random.uniform(2.5, 4), 1)
            
        # température  extérieur
        if external_temperature < 8 or external_temperature > 14:
            print(f"[ETL] Automate {automate_number} external_temperature {external_temperature} is not in (8; 14), proceed to modify this value")
            external_temperature = round(random.uniform(8, 14), 1)
            
        
        if milk_weight < 3512 or milk_weight > 4607:
            print(f"[ETL] Automate {automate_number} milk_weight {milk_weight} is not in (3512; 4607), proceed to modify this value")
            milk_weight = random.randrange(3512, 4607)
            
        
        query = f""" INSERT INTO bon_beurre (
            unit_number,
            automate_number,
            automate_type,
            tank_temperature,
            external_temperature,
            milk_weight,
            ph,
            k_plus,
            nacl,
            salmonella,
            e_coli,
            listeria,
            produced_at
        ) VALUES (
            {unit_number},
            {automate_number},
            '{automate_type}',
            {tank_temperature},
            {external_temperature},
            {milk_weight},
            {ph},
            {k_plus},
            {nacl},
            {salmonella},
            {e_coli},
            {listeria},
            '{produced_at}'
        );
        """
        
        cursor.execute(query)
        mydb.commit()


if __name__ == '__main__':

    print("Collect")
    collect()

        
        




