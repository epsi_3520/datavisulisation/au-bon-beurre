import mysql.connector


def createDB():
    mydb = mysql.connector.connect(
          host="db",
          port=3306,
          user="root",
          password="root",
          database="bon_beurre"
    )
    
    cursor = mydb.cursor()
    
    # Delete table if exist and recreate it
    query = "DROP TABLE IF EXISTS bon_beurre;"
    cursor.execute(query)
    
    query = """CREATE TABLE bon_beurre (
    	id INT AUTO_INCREMENT PRIMARY KEY,
    	unit_number INT,
    	automate_number INT,
    	automate_type VARCHAR (20),
    	tank_temperature FLOAT,
    	external_temperature FLOAT,
    	milk_weight INT,
    	ph FLOAT,
    	k_plus INT,
    	nacl FLOAT,
    	salmonella INT,
    	e_coli INT,
    	listeria INT,
        produced_at TIMESTAMP,
    	collected_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );
    """
    
    cursor.execute(query)
    
    cursor.close()
    mydb.close()
    

if __name__ == '__main__':
    print("******************** CREATE DB ********************")
    createDB()
    print("******************** SUCCESS ********************")
          
          